###### Application launch ######
To run the app, enter "docker-compose up --build"

###### You can use queries ######

POST http://localhost:3001/user
{
    "user_name": "Kirill",
    "password": "132",
    "role": "admin",
    "email": "kirill@ukr.net"
}

DELETE http://localhost:3001/user/2

GET http://localhost:3001/user

PUT http://localhost:3001/user/1
{
    "user_name": "Kirill",
    "role": "super-admin",
    "email": "kirill@ukr.net",
    "password": "123456"
}




POST http://localhost:3001/address
{
    "userId": "1",
    "country": "UA",
    "city": "Kiev",
    "street": "Soborniy",
    "building": "783579"
}

GET http://localhost:3001/address
DELETE http://localhost:3001/address/2